package epam.sorting;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortingAppTest {

    @Parameterized.Parameters()
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new int[]{1, 3, 2}, new int[]{1, 2, 3}},        // Normal case
                {new int[]{}, new int[]{}},                      // Empty array (zero arguments)
                {new int[]{5}, new int[]{5}},                    // Single element array (one argument)
                {new int[]{7, 2, 9, 1, 5, 4, 8, 6, 3, 10},       // Max allowed arguments (ten arguments)
                        new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}},
//                {new int[]{4, 1, 7, 2, 9, 6, 3, 8, 5, 10, 11},  // More than 10 arguments (corner case)
//                        new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}}
        });
    }

    private final int[] input;
    private final int[] expected;

    public SortingAppTest(int[] input, int[] expected) {
        this.input = input;
        this.expected = expected;
    }

    @Test
    public void testSort() {
        SortingApp.sort(input);
        assertArrayEquals(expected, input);
    }
}