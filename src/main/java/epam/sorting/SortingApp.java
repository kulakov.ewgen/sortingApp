package epam.sorting;

import java.util.*;


public class SortingApp {

    /**
     * Sorts the given array of strings in ascending order and prints the result.
     *
     * @param arrInt The array of integers to be sorted.
     */
    public static void sort(int[] arrInt) {
        if (arrInt.length > 10){
            throw new IllegalArgumentException("There must be up to 10 arguments");
        }
        Arrays.sort(arrInt);
        System.out.println(Arrays.toString(arrInt));
    }

    public static void main(String[] args) {
        int[] arrInt1 = {1, 3, 2};
        sort(arrInt1);

        int[] arrInt2 = {};
        sort(arrInt2);

        int[] arrInt3 = {5};
        sort(arrInt3);

        int[] arrInt4 = {7, 2, 9, 1, 5, 4, 8, 6, 3, 10};
        sort(arrInt4);

//        int[] arrInt5 = {4, 1, 7, 2, 9, 6, 3, 8, 5, 10, 11};
//        sort(arrInt5);
    }
}